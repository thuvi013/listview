import 'package:flutter/material.dart';
import 'package:task_02/map_page.dart';
import 'package:task_02/models/data_model.dart';

class DetailPage extends StatefulWidget {
  final DataInfo dataInfo;
  const DetailPage(this.dataInfo, {super.key});

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const BackButton(
     color: Colors.black
   ), 
        title: const Text("Details",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold)),
        centerTitle: true,
        backgroundColor: Colors.teal[400],
        actions: [
          IconButton(
            color: Colors.black,
            icon: const Icon(Icons.location_on,color: Colors.black,),
            tooltip: 'Show Snackbar',
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => MapPage(
                    widget.dataInfo.latitude.toString(),
                    widget.dataInfo.longitude.toString(),
                  ),
                ),
              );
            },
          ),
        ],
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Container(
                height: 200,
                width: 200,
                decoration: const BoxDecoration(
                  color: Colors.black,
                  shape: BoxShape.rectangle,
                  image: DecorationImage(image: NetworkImage("https://picsum.photos/250?image=9"))
                ),
                // child: Image.network(
                //   widget.dataInfo.image!.large.toString(),
                // ),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                widget.dataInfo.title.toString(),
                style: const TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 10,
                  right: 10,
                ),
                child: Text(
                  widget.dataInfo.description.toString(),
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
